########################################################################
# Copyright 2018 Jason Alan Smith
#
# This file is part of Quality Operations Center.
#
# Quality Operations Center is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quality Operations Center is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quality Operations Center.
# If not, see <https://www.gnu.org/licenses/>.
########################################################################


from flask import Flask, render_template
from flask_marshmallow import Marshmallow
from flask_logconfig import LogConfig

import logging
from logging.handlers import RotatingFileHandler

from flask_sqlalchemy import SQLAlchemy

from sqlalchemy import text


logcfg = LogConfig()
db = SQLAlchemy()
ma = Marshmallow()



def create_app(config_filename):
    app = Flask(__name__)
    app.config.from_pyfile(config_filename)

    # TODO: Understand benefits of Flask-LogConfig if using only
    #       basics of module. Determine this line needed or not.
    # logcfg.init_app(app)

    logcfg.init_app(app)
    
    logger = logging.getLogger('qops_desktop')
    
    logger.debug('Application logging has been initialized.')
    logger.debug('Quality Operations Center application booting.')

    # TODO: Understand why both lines needed here.
    # db.app = app
    # db.init_app(app)

    logger.debug('Initialize database with application object.')
    db.app = app
    db.init_app(app)
    ma.init_app(app)

    
    logger.debug('Initialize Flask Blueprints.')
    load_blueprints(app, logger)


    # Drop objects created using raw SQL.
    # TODO: Refactor this section to own function.
    logger.debug('About to drop objects created using raw SQL.')
    logger.debug('Dropping vw_backlogs from QOps database.')
    # IMPORTANT! DO NOT REPLACE "engine" WITH "session"!
    # BAD THINGS WILL HAPPEN!
    qops = db.get_engine(bind='qops')
    #qops.execute(text('DROP VIEW IF EXISTS vw_backlogs'))
    logger.debug('Dropped vw_backlogs from QOps database.')
    logger.debug('Finished dropping objects created using raw SQL.')


    # Drop all model objects from QOps database.
    # TODO: Refactor this section to own function.
    # logger.debug('About to drop all model objects from QOps database.')
    # logger.debug('Dropping all model objects from QOps database.')
    
    # Permanently disable dropping all tables in QOps database.
    # Time to start eating my own dog food, and start using
    # database migrations as the proper way to handle database changes.
    db.drop_all()
    
    # logger.debug('Dropped all model objects from QOps database.')
    # logger.debug('Finished dropping all model objects from QOps database.')


    # Create all model objects in QOps database.
    # TODO: Refactor this section to own function.
    logger.debug('About to create all model objects in QOps database.')
    logger.debug('Creating all model objects in QOps database.')
    db.create_all()
    logger.debug('Created all model objects in QOps database.')
    logger.debug('Finished creating all model objects in QOps database.')


    # Truncate table system_log in QOpsLog database.
    # TODO: Refactor this section to own function.
    logger.debug('About to truncate table system_log in QOpsLog database.')
    logger.debug('Truncating table system_log in QOpsLog database.')
    qopslog = db.get_engine(bind='qopslog')
    qopslog.execute(text('TRUNCATE TABLE system_log'))
    logger.debug('Truncated table system_log in QOpsLog database.')
    logger.debug('Finished truncating table system_log in QOpsLog database.')




    # Return configured application object.
    logger.debug('About to return configured application object.')
    logger.debug('Returning configured application object.')
    logger.debug('Now transferring control over to Home service.')
    logger.debug('Logging now transferring to system_log table in QOpsLog database.')
    return app
    # TODO: Research if you can log from WSGI file using same
    #       logger configured here.  This way you can log the
    #       successful return of the application object.  If
    #       this is not possible, then fall back to logging
    #       in Home service with assumption that if we reach
    #       that point then the application object was indeed
    #       successfully returned from here.


# TODO: Sequence the import and registration of blueprints
#       in a reasonable order.
def load_blueprints(app, logger):
    logger.debug('About to import blueprints.')
    
    logger.debug('Importing Backlog blueprint.')
    from backlog_services.backlog_api.controllers import backlog_api \
        as \
        backlog_api
    logger.debug('Imported Backlog blueprint.')

    logger.debug('Finished importing blueprints.')
    
    
    logger.debug('About to register blueprints.')

    logger.debug('Registering Home blueprint.')
    app.register_blueprint(backlog_api, url_prefix='/api')
    logger.debug('Registered Home blueprint.')
    
    logger.debug('Finished registering blueprints.')

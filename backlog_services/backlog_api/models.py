########################################################################
# Copyright 2018 Jason Alan Smith
#
# This file is part of Quality Operations Center.
#
# Quality Operations Center is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quality Operations Center is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quality Operations Center.
# If not, see <https://www.gnu.org/licenses/>.
########################################################################
"""The Backlog Service's data models.

These entities must contain one, and only one, backlog:

Customer
Iteration
Person
Product
Project
Release
Team

"""

from marshmallow import Schema, fields, pre_load
from marshmallow import validate
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

from backlog_services import db
from backlog_services import ma

class Base(db.Model):
    __abstract__ = True
    __bind_key__ = 'qops'

    id = db.Column(db.Integer, primary_key = True)
    created_on = db.Column(db.DateTime, default=db.func.now())
    updated_on = db.Column(db.DateTime, default=db.func.now(), onupdate=db.func.now())


class AddUpdateDelete():
    def add(self, resource):
        db.session.add(resource)
        return db.session.commit()

    def update(self):
        return db.session.commit()

    def delete(self, resource):
        db.session.delete(resource)
        return db.session.commit()


class ProductBacklog(Base, AddUpdateDelete):
    """Data model for a backlog."""

    identifier = db.Column(db.String(25), unique=True, nullable=False)
    name = db.Column(db.String(50), unique=True, nullable=False)
    description = db.Column(db.String(512), nullable=True)


class ProductBacklogSchema(ma.Schema):
    """Schema for a backlog."""
    id = fields.Integer(dump_only=True)
    identifier = fields.String(required=True,
                               validate=validate.Length(1, 25))
    name = fields.String(required=True, validate=validate.Length(1, 50))
    description = fields.String(required=False,
                                validate=validate.Length(1, 512))

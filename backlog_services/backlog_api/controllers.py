########################################################################
# Copyright 2018 Jason Alan Smith
#
# This file is part of Quality Operations Center.
#
# Quality Operations Center is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quality Operations Center is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quality Operations Center.
# If not, see <https://www.gnu.org/licenses/>.
########################################################################
"""
FIXME.

FIXME.
"""

from flask import Blueprint, request, jsonify, make_response
from flask_restful import Api, Resource
from sqlalchemy.exc import SQLAlchemyError
import status

from .models import ProductBacklog

from .models import ProductBacklogSchema


from backlog_services import db
from backlog_services import ma

backlog_api = Blueprint('backlog_api', __name__)
backlog_schema = ProductBacklogSchema()
api = Api(backlog_api)


class BacklogResource(Resource):
    def get(self, id):
        backlog = ProductBacklog.query.get_or_404(id)
        result = backlog_schema.dump(backlog).data
        return result

    def patch(self, id):
        backlog = ProductBacklog.query.get_or_404(id)
        backlog_dict = request.get_json(force=True)
        if 'name' in backlog_dict:
            backlog.name = backlog_dict['name']
        dumped_message, dump_errors = backlog_schema.dump(backlog)
        if dump_errors:
            return dump_errors, 400
        validate_errors = backlog_schema.validate(dumped_message)
        # errors = message_schema.validate(data)
        if validate_errors:
            return validate_errors, 400
        try:
            backlog.update()
            return self.get(id)
        except SQLAlchemyError as e:
            db.session.rollback()
            resp = jsonify({"error": str(e)})
            return resp, 400

    def delete(self, id):
        backlog = ProductBacklog.query.get_or_404(id)
        try:
            delete = backlog.delete(backlog)
            response = make_response()
            return response, 204
        except SQLAlchemyError as e:
            db.session.rollback()
            resp = jsonify({"error": str(e)})
            return resp, 401





class BacklogListResource(Resource):
    def get(self):
        backlogs = ProductBacklog.query.all()
        result = backlog_schema.dump(backlogs, many=True).data
        return result

    def post(self):
        request_dict = request.get_json()
        if not request_dict:
            response = {'message': 'No input data provided'}
            return response, 400
        errors = backlog_schema.validate(request_dict)
        if errors:
            return errors, 400
        try:
            backlog = Backlog(
                name=request_dict['name'])
            backlog.add(backlog)
            query = Backlog.query.get(backlog.id)
            result = backlog_schema.dump(query).data
            return result, 201
        except SQLAlchemyError as e:
            db.session.rollback()
            resp = jsonify({"error": str(e)})
            return resp, 400



api.add_resource(BacklogListResource, '/backlogs/')
api.add_resource(BacklogResource, '/backlogs/<int:id>')
